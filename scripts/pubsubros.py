#!/usr/bin/env python
import time
import rospy
from std_msgs.msg import Float32
from std_msgs.msg import Int32
from sensor_msgs.msg import RelativeHumidity
 

def callback(readHumidity):
    motor(readHumidity)

 

def motor(readHumidity):
    pub=rospy.Publisher('Servo', Int32, queue_size=10) #rospy.Publisher(Topic name, Type, queue_size )
    #rospy.init_node('motor',anonymous=True) #rospy.init_node(Node name,anonymous=True)
    rate=rospy.Rate(10) #10Hz
    if  readHumidity>160:
        pub.publish(0)  
        rospy.loginfo(readHumidity)
                 
    else:
        pub.publish(1)
        rospy.loginfo(readHumidity)  
    rate.sleep()

 

    
def listener():
    rospy.init_node('listener', anonymous=True) #anonymus let us have many node named the same way running simultaneously
    rospy.Subscriber('humidity', RelativeHumidity,callback) #Callback name of the function you need to call when you get data
    rospy.spin() #keeps the program running until the node is stopped

 


if __name__=='__main__':
    listener()
