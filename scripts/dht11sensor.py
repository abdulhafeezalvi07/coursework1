
#include "DHT.h"
#include <ros.h>
#include <sensor_msgs/RelativeHumidity.h>
#include <std_msgs/Int32.h>
#include <Servo.h>
#define DHTPIN A5    // what pin we're connected to
#define DHTTYPE DHT11   // DHT 22  (AM2302)
DHT dht(DHTPIN, DHTTYPE); //// Initialize DHT sensor for normal 16mhz Arduino
int pos = 0 ;
int chk;
ros::NodeHandle  nh;

Servo myservo;
void messageCb( const std_msgs::Int32& movewiper) {
  if (movewiper.data == 1) {
    myservo.write(180);
  }
  else {



    myservo.write(0);
    delay(15);

  }
}

ros::Subscriber<std_msgs::Int32> sub("Servo", messageCb );

sensor_msgs::RelativeHumidity humidity_msg;
ros::Publisher humidity("humidity", &humidity_msg);

//char hello[13] = "hello world!";

void setup()
{
  myservo.attach(9); //tell the arduino where the motor is attached and to control it
  nh.initNode(); //for initializing
  nh.advertise(humidity);
  nh.subscribe(sub);
}

void loop()
{
  
  humidity_msg.relative_humidity = dht.readHumidity();
  
  if (dht.readHumidity()>180){
    for (pos = 0; pos <= 180; pos += 1) {

      myservo.write(pos);
      delay(15);
    }

  }

  else {



    myservo.write(0);
    delay(15);

  }
  delay(500);
  
  humidity.publish( &humidity_msg );
  nh.spinOnce();
  delay(500);
}
